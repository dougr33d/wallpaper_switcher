#!/usr/bin/perl
use warnings;
use strict;
use Storable;

######################################
# Configuration constants
my $IMGDIR      = "/home/reed/old_home/Wallpapers";
my $BIN_CONVERT = "/usr/bin/convert";

# <OPTS> should be -channel Red or -type brightnessscale
my $COLOR_CMD   = "$BIN_CONVERT <IMAGE> <OPTS> -format \"%[mean]\" info:";

my $BG_CMD      = "nitrogen --set-zoom-fill <IMAGE>";

$ENV{"DISPLAY"}=":0.0";

# Config ends here
######################################

sub get_images {
  my $imglst = `ls $IMGDIR`;
  chomp $imglst;
  my @tmp_images = split(/\n/, $imglst);

  my @images = ();
  foreach my $i (@tmp_images) {
    if ($i =~ /\.(jpg|JPG|png|PNG|jpeg|JPEG|bmp|BMP|gif|GIF)$/) {
      push(@images, "$IMGDIR/$i");
    }
  }
  return @images;
}

sub image_to_brightness {
  my ($fn) = @_;

  my $cmd = $COLOR_CMD;
  $cmd =~ s/<IMAGE>/$fn/;


  my  $cmd_red   = $cmd; $cmd_red   =~ s/<OPTS>/-channel Red    / ; 
  my  $cmd_green = $cmd; $cmd_green =~ s/<OPTS>/-channel Green  / ;
  my  $cmd_blue  = $cmd; $cmd_blue  =~ s/<OPTS>/-channel Blue   / ;
  my  $cmd_brightness  = $cmd; $cmd_brightness  =~ s/<OPTS>/-type Grayscale / ;

  my $red   =0;#`$cmd_red`   ; chomp $red;
  my $green =0;#`$cmd_green` ; chomp $green;
  my $blue  =0;#`$cmd_blue`  ; chomp $blue;
  my $brightness  = `$cmd_brightness`  ; chomp $brightness;

  my %image_properties = (
    name  => $fn,
    red   => $red,
    green => $red,
    blue  => $blue,
    brightness  => $brightness
  );

  printf("%s\n\tr:%d\n\tg:%d\n\tb:%d\n\tgr:%d\n\n", $fn, $red, $green, $blue, $brightness);
  return \%image_properties;
}                                            

sub image_to_background {
  my ($fn) = @_;
  my $cmd = $BG_CMD;
  $cmd =~ s/<IMAGE>/$fn/;
  `$cmd`;
  print "$cmd\n";
  print "Changed wallpaper to $fn\n";

}


# Try to retrieve brightness data structure from disk
my @props = ();
my @oldprops = ();
eval {
  @oldprops = @{ retrieve("$IMGDIR/.walldb") };
  1;
};

# Only look at brightness for now.
my @min_max_brightness = (999999,0);
foreach my $i (get_images()) {
  # Skip files that have already been loaded
  my $hp;
  my $seen_it = 0;

  foreach my $p (@oldprops) {
    if ($p->{"name"} eq $i) {
      push(@props, $p);
      $hp = $p;
      $seen_it = 1;
    }
  }

  # New files need the works
  if ($seen_it == 0) {
    $hp = image_to_brightness($i);
    push(@props, $hp);
  }
  $min_max_brightness[0] = $hp->{"brightness"} if ($hp->{"brightness"} < $min_max_brightness[0]) ;
  $min_max_brightness[1] = $hp->{"brightness"} if ($hp->{"brightness"} > $min_max_brightness[1]) ;
}

printf("Min brightness observed: %d\n", $min_max_brightness[0]);
printf("Max brightness observed: %d\n", $min_max_brightness[1]);

my $delta = $min_max_brightness[1] - $min_max_brightness[0];
              
my $hour = `date +"%H"`;
chomp $hour;

#sort { $a->{"brightness"} <=> $b->{"brightness"} } @props;
my $T1 = $min_max_brightness[0] + ($delta * 0.200);
my $T2 = $min_max_brightness[0] + ($delta * 0.650);

my @images = ();
if ( ($hour < 6) || ($hour > 19) ) { # night time
  foreach my $img (@props) {
    push(@images, $img) if($img->{"brightness"} < $T1);
  }
} elsif ( ($hour < 8) || ($hour > 17) ) { # dusk/dawn
  foreach my $img (@props) {
    push(@images, $img) if ( ($img->{"brightness"} >= $T1) 
                          && ($img->{"brightness"} < $T2));
  }
} else { #midday
  foreach my $img (@props) {
    push(@images, $img) if ($img->{"brightness"} >= $T2);
  }
}

my $use_image = @images[int(rand(@images))];
image_to_background($use_image->{"name"});

print "Saving db file back to $IMGDIR/.walldb\n";
store(\@props, "$IMGDIR/.walldb") or die "Could not save DB file!";
